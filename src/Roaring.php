<?php

namespace Finpaa\KYC;

use Finpaa\Finpaa;

class Roaring
{
	private static $sequence;
	private static $products;
	private static $ROARING_TOKEN;

	private static function selfConstruct()
	{
		$finpaa = new Finpaa();
		self::$sequence = $finpaa->Sweden()->SandBox()->Roaring()->start();
		self::$products = $finpaa->Sweden()->SandBox()->Roaring_SE()->Products();
	}

	private static function getSequenceCode($name = 'sequence')
	{
		if(self::$$name) {
			return self::$$name;
		}
		else {
			self::selfConstruct();
			return self::getSequenceCode($name);
		}
	}

	private static function executeMethod($methodCode, $alterations, $returnPayload = false)
	{
		$response = Finpaa::executeTheMethod($methodCode, $alterations, $returnPayload);
		return array('error' => false, 'response' => json_decode(json_encode($response), true));
	}

	private static function executeSequenceMethod($code, $methodIndex, $alterations, $name, $returnPayload)
	{
		$sequence = Finpaa::getSequenceMethods($code);
		if (isset($sequence->SequenceExecutions)) {
			if($methodIndex < count($sequence->SequenceExecutions))
			{
				$response = Finpaa::executeTheMethod(
					$sequence->SequenceExecutions[$methodIndex]->methodCode, $alterations, $returnPayload
				);
				return array('error' => false, 'response' => json_decode(json_encode($response), true));
			}
			else {
				return array('error' => true, 'message' => $name . ' method Failed -> Index out of bound',
					'methodIndex' => $methodIndex, 'methodsCount' => count($sequence->SequenceExecutions));
			}
		}
		else
		{
			return array('error' => true,'message' => $name .' -> No sequence executions', 'response' => json_decode(json_encode($sequence), true));
		}
	}

	static function getRoaringAuthToken()
	{
		if(self::$ROARING_TOKEN)
		{
			return self::$ROARING_TOKEN;
		}
		else
		{
			$alterations[]['headers']['Authorization'] = "Basic " . base64_encode(env("ROARING_CLIENT_ID") . ":" . env("ROARING_CLIENT_SECRET"));
			$response = self::executeSequenceMethod(
				self::getSequenceCode(), 0, $alterations, 'getRoaringAuthToken', false);
			if(!$response['error'] && isset($response['response']))
				self::$ROARING_TOKEN = "Bearer " . $response['response']['access_token'];
			else self::$ROARING_TOKEN = "Unauthorized";
			return self::getRoaringAuthToken();
		}
	}

	static function getPersonInformationByPersonalNumber($personalNumber): array|string
	{
		$alterations[]['headers']['Authorization'] = self::getRoaringAuthToken();
		$alterations[]['query']['personalNumber'] = $personalNumber;
		$response = self::executeSequenceMethod(self::getSequenceCode(), 1, $alterations, 'getPersonInformationByPersonalNumber', false);
		if(!$response['error'] && isset($response['response']['posts'][0]))
		{
			$post = $response['response']['posts'][0];

			return array(
				"personalNumber" => $post['personalNumber'],
				"details" =>  $post['details'],
				"address" => $post['address'],
			);
		}
		else return null;
	}

	static function getCompanyInformationByPersonalNumber($personalNumber)
	{
		$alterations[]['headers']['Authorization'] = self::getRoaringAuthToken();
		$alterations[]['path']['personalNumber'] = $personalNumber;
		$response = self::executeSequenceMethod(
			self::getSequenceCode(), 2, $alterations, 'getCompanyInformationByPersonalNumber', false
		);
		if(!$response['error'] && isset($response['response']['engagements']))
		{
			$companies = array();
			foreach ($response['response']['engagements'] as $engagement)
			{
				$res = self::getCompanyByCompanyId($engagement['companyId']);
				if($res) $res = $res['records'];
				$companies[] = array(
					'records' => $res,
					'engagement' => $engagement
				);
			}
			return $companies;
		}
		else return null;
	}

	static function getCompanyByCompanyId($companyId)
	{
		$alterations[]['headers']['Authorization'] = self::getRoaringAuthToken();
		$alterations[]['path']['companyId'] = $companyId;
		$response = self::executeSequenceMethod(
			self::getSequenceCode(), 4, $alterations, 'getCompanyByCompanyId', false);
		if(!$response['error'] && isset($response['response']))
			return $response['response'];
		return null;
	}

	static function getCompanyOwnerStructureByCompanyId($companyId)
	{
		$alterations[]['headers']['Authorization'] = self::getRoaringAuthToken();
		$alterations[]['path']['companyId'] = $companyId;
		$alterations[]['query']['toDate'] = null;
		$alterations[]['query']['fromDate'] = null;
		$response = self::executeSequenceMethod(
			self::getSequenceCode(), 6, $alterations, 'alt-beneficial-owners', false
		);
		if(!$response['error'] && isset($response['response']))
			return $response['response'];
		return null;
	}

	static function getBeneficialOwnerByCompanyId($companyId)
	{
		$methodCode = self::getSequenceCode('products')->beneficialOwner()->getBeneficialOwnersForCompany();

		$alterations[]['headers']['Authorization'] = self::getRoaringAuthToken();
		$alterations[]['path'] = array(
			'companyId' => $companyId,
		);

		$response = self::executeMethod($methodCode, $alterations);

		if(!$response['error'] && isset($response['response']['records'][0]))
			return $response['response']['records'][0];
		return null;
	}

	static function getPep($personalNumber, $countryCode)
	{
		$methodCode = self::getSequenceCode('products')->politicallyExposedPerson()->searchForSinglePep();

		$alterations[]['headers']['Authorization'] = self::getRoaringAuthToken();
		$alterations[]['query'] = array(
			'pep' => null,
			'rca' => null,
			'countryCode' => $countryCode,
			'personalNumber' => $personalNumber,
			'firstName' => null,
			'lastName' => null,
			'birthDate' => null,
			'fullName' => null,
			'gender' => null,
		);

		$response = self::executeMethod($methodCode, $alterations);

		if(!$response['error'] && isset($response['response']['hits']))
			return $response['response']['hits'];
		return null;
	}

	static function getGlobalPep($detail)
	{
		$methodCode = self::getSequenceCode('products')->globalPoliticallyExposedPerson()->searchForSingleGlobalPep();

		$alterations[]['headers']['Authorization'] = self::getRoaringAuthToken();
		$alterations[]['query'] = array(
			'firstName' => $detail->first_name ?? '',
			'surName' => $detail->last_name ?? '',
			'fullName' => null,
			'birthDate' => null,
			'gender' => null,
			'address' => null,
			'country' => null,
		);

		$response = self::executeMethod($methodCode, $alterations);

		if(!$response['error'] && isset($response['response']['records']))
			return $response['response']['records'];
		return $response;
	}

	static function getSanctions($detail, $countryCode = null, $company = false)
	{
		$methodCode = self::getSequenceCode('products')->sanctionsLists()->searchSanctionsLists();

		$alterations[]['headers']['Authorization'] = self::getRoaringAuthToken();

		if($company)
		{
			$alterations[]['query'] = array(
				'sanctionOrg' => $detail->company_id,
				'programme' => null,
				'country' => $countryCode,
				'name' => $detail->company_name,
				'birthDate' => null,
				'gender' => null,
			);
		}
		else
		{
			$gender = 'Unknown';
			if($detail->gender == 'M')
				$gender = 'Male';
			if($detail->gender == 'F')
				$gender = 'Female';

			$alterations[]['query'] = array(
				'sanctionOrg' => null,
				'programme' => null,
				'country' => $countryCode,
				'name' => $detail->full_name,
				'birthDate' => date("Y-m-d", strtotime($detail->birth_date)),
				'gender' => $gender,
			);
		}


		$response = self::executeMethod($methodCode, $alterations);

		if(!$response['error'] && isset($response['response']['sanctionsListsRecords']))
			return $response['response']['sanctionsListsRecords'];
		return null;
	}

	static function getSignatoryRights($companyId, $personalNumber)
	{
		$methodCode = self::getSequenceCode('products')->signatoryRight()->checkIfPersonIsEntitledToSignCompany();

		$alterations[]['headers']['Authorization'] = self::getRoaringAuthToken();
		$alterations[]['query'] = array(
			'asOfDate' => date('Y-m-d'),
		);
		$alterations[]['path'] = array(
			'companyId' => $companyId,
			'personalNumber' => $personalNumber
		);

		$response = self::executeMethod($methodCode, $alterations);

		if(!$response['error'] && isset($response['response']['records']))
			return $response['response']['records'];
		return $response;
	}

	static function getSigningCombination($companyId, $personalNumber)
	{
		$methodCode = self::getSequenceCode('products')->signingCombinations()->fetchSigningCombinationsFromCompany();

		$alterations[]['headers']['Authorization'] = self::getRoaringAuthToken();
		$alterations[]['query'] = array(
			'personalNumber' => $personalNumber,
			'asOfDate' => date('Y-m-d'),
		);
		$alterations[]['path'] = array(
			'companyId' => $companyId,
		);

		$response = self::executeMethod($methodCode, $alterations);

		if(!$response['error'] && isset($response['response']['records'][0]))
			return $response['response']['records'][0];
		return $response;
	}
}
